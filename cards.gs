/**
* Builds the search interface for looking up people.
*
* @param {string} opt_error - Optional message to include (typically when
*    contextual search failed.)
* @return {Card} Card to display
*/
function buildSearchCard_(opt_error) {
  var banner = CardService.newImage()
  .setImageUrl('https://storage.googleapis.com/addons-statics/find-mails.png');

  var searchField = CardService.newTextInput()
  .setFieldName("query")
  .setHint("Email address only")
  .setTitle("Search for emails");

  var onSubmitAction = CardService.newAction()
  .setFunctionName("onSearch")
  .setLoadIndicator(CardService.LoadIndicator.SPINNER);

  var submitButton = CardService.newTextButton()
  .setText("Search")
  .setOnClickAction(onSubmitAction);

  var section = CardService.newCardSection()
  .addWidget(banner)
  .addWidget(searchField)
  .addWidget(submitButton);

  if (opt_error) {
    var message = CardService
    .newTextParagraph()
    .setText("Note: " + opt_error);
    section.addWidget(message);
  }


  return CardService.newCardBuilder().addSection(section).build();
}

/**
* Builds a card for displaying a list of messages
*
* @param {Object[]} messages - Array of messages
* @return {Card} Card to display
*/
function buildMessagesListCard_(messages) {
  var resultsSection = CardService.newCardSection();
  messages.forEach(function(message) {
    var title = message.title;
    var action = CardService.newAction().setFunctionName('openMessageLink')
    .setLoadIndicator(CardService.LoadIndicator.SPINNER)
    .setParameters({link: message.link});

    var personSummaryWidget = CardService.newKeyValue()
    .setContent(title)
    .setBottomLabel(message.date)
    .setIcon(CardService.Icon.EMAIL)
    .setOnClickOpenLinkAction(action);
    resultsSection.addWidget(personSummaryWidget);
  });
  return CardService.newCardBuilder()
  .addSection(resultsSection)
  .build();
}