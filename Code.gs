// See https://github.com/contributorpw/lodashgs
var _ = LodashGS.load();

/**
* Renders the home page for the add-on. Used in all host apps when
* no context selected.
*
* @param {Object} event - current add-on event
* @return {Card[]} Card(s) to display
*/
function onHomePage(event) {
  var card = buildSearchCard_();
  return [card];
}

/**
* Renders the contextual interface for a Gmail message.
*
* @param {Object} event - current add-on event
* @return {Card[]} Card(s) to display
*/
function onGmailMessageSelected(event) {
  var from = extractSenderFromMessage_(event);
  if (!from) {
    var card = buildSearchCard_("No message found for current sender.");
    return [card];
  }
  var messages = getMessagesFromPerson(from);
  var card = buildMessagesListCard_(messages)
  return [card];
}

function getMessagesFromPerson(from){
  Logger.log("Searching to from: %s", from);
  var threads = GmailApp.search('!is:chat from:'+from, 0, 20);
  var messageList = [];
  threads.forEach(function(thread) {
    var message = thread.getMessages()[0];
    messageList.push({title:thread.getFirstMessageSubject()?thread.getFirstMessageSubject():"(no subject)",
                      link:thread.getPermalink(),
                      date:message.getDate()});
  });  return messageList;
}

function openMessageLink(event){
  return CardService.newActionResponseBuilder().setOpenLink( CardService.newOpenLink().setUrl(event.parameters.link) ).build();
}

function onSearch(event) {
  if (!event.formInputs || !event.formInputs.query) {
    var notification = CardService.newNotification()
    .setText("Enter a email address before searching.");
    return CardService.newActionResponseBuilder()
    .setNotification(notification)
    .build();
  }

  var from =  event.formInputs.query[0];
  var messages = getMessagesFromPerson(from);

  if (!messages || messages.length == 0) {
    var notification = CardService.newNotification().setText("No messages found.");
    return CardService.newActionResponseBuilder()
    .setNotification(notification)
    .build();
  }

  var card = buildMessagesListCard_(messages);
  var navigation = CardService.newNavigation().pushCard(card);
  return CardService.newActionResponseBuilder()
  .setNavigation(navigation)
  .build();
}

/**
* Extracts email addresses from the selected Gmail message. Grabs only to email address from headers.
*
* @param {Object} event - current add-on event
* @return {string[]} Array of email addresses.
*/
function extractSenderFromMessage_(event) {
  // Fetch currently selected message
  var accessToken = event.messageMetadata.accessToken;
  var messageId = event.messageMetadata.messageId;
  GmailApp.setCurrentMessageAccessToken(accessToken);
  var message = GmailApp.getMessageById(messageId);

  if (!message) {
    return [];
  }

  // Parse/emit any email addresses in the to/cc/from headers
  var splitEmailsRegexp = /\b[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}\b/gi;
  var from = message.getFrom().match(splitEmailsRegexp);

  return from;
}

/**
 * Filter email addresses to include only those in the same
 * domain and excluding the current user.
 *
 * @param {string[]} emails - Array of email addresses
 * @return {string[]}
 */
function filterAndSortEmails_(emails) {
  if (!emails) {
    return [];
  }

  var userEmail = Session.getActiveUser().getEmail();
  var domain = userEmail.slice(userEmail.indexOf('@') + 1);

  emails = emails.filter(function(email) {
    return _.endsWith(email, domain) && email != userEmail;
  });
  emails = _.uniq(emails);
  return emails.sort();
}

/**
* Extracts email addresses from the selected Gmail message. Grabs all emails
* from the to/cc/from headers.
*
* @param {Object} event - current add-on event
* @return {string[]} Array of email addresses.
*/
function extractEmailsFromMessage_(event) {
  // Fetch currently selected message
  var accessToken = event.messageMetadata.accessToken;
  var messageId = event.messageMetadata.messageId;
  GmailApp.setCurrentMessageAccessToken(accessToken);
  var message = GmailApp.getMessageById(messageId);

  if (!message) {
    return [];
  }

  // Parse/emit any email addresses in the to/cc/from headers
  var splitEmailsRegexp = /\b[A-Z0-9._%+-]+@(?:[A-Z0-9-]+\.)+[A-Z]{2,6}\b/gi;
  var emails = _.union(
    message.getTo().match(splitEmailsRegexp),
    message.getCc().match(splitEmailsRegexp),
    message.getFrom().match(splitEmailsRegexp)
  );

  // Remove any +suffixes in the user name portion to get the canonical email
  var normalizeRegexp = /(.*)\+.*@(.*)/;
  emails = emails.map(function(email) {
    return email.replace(normalizeRegexp, "$1@$2");
  });

  return filterAndSortEmails_(emails);
}