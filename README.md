# SameSender-GmailAddon

A Gmail Addon which will search for emails from the current sender opened message.

You just need to open your email, and click on the Filter button right on Gmail Addons bar. Done, the Same Sender will show you the last 20 emails from this sender.

![Same Sender screenshot](https://gitlab.com/google-addons/samesender-gmailaddon/-/wikis/uploads/aafca951303f957cc4093a2206caad27/SameSenderPic.jpg)
